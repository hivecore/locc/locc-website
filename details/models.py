from django.db import models

from wagtail.models import Page, Orderable
from wagtail.fields import RichTextField
from wagtail.admin.edit_handlers import InlinePanel
from wagtail.admin.panels import FieldPanel
from modelcluster.fields import ParentalKey
from wagtail.images.edit_handlers import ImageChooserPanel

class DetailItems(Orderable):
    page = ParentalKey("details.DetailPage", related_name="detail_item")
    photo =  models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    title = models.CharField(max_length=250, blank=True)
    description = RichTextField(blank=True)

    panels = [
        ImageChooserPanel("photo"),
        FieldPanel("title"),
        FieldPanel("description")
    ]

class DetailPage(Page):
    cover_photo =  models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )


    content_panels = Page.content_panels + [
        FieldPanel('cover_photo', classname="full"),
        InlinePanel("detail_item")
    ]
