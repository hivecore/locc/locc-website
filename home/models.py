from django.db import models

from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel

class HomePage(Page):

    max_count = 1

    welcome_title = models.CharField(max_length=250, blank=True)
    welcome_message = RichTextField(blank=True)
    president_title = models.CharField(max_length=250, blank=True)
    president_message = RichTextField(blank=True)
    igp_title = models.CharField(max_length=250, blank=True)
    igp_message = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        FieldPanel('welcome_title', classname="full"),
        FieldPanel('welcome_message', classname="full"),
        FieldPanel('president_title', classname="full"),
        FieldPanel('president_message', classname="full"),
        FieldPanel('igp_title', classname="full"),
        FieldPanel('igp_message', classname="full"),
    ]
